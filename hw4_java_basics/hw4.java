package Homework;

import java.util.Scanner;

public class hw4_java_basics {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please, enter recipient's name: ");
        String teamlead = sc.nextLine();
        while (!teamlead.matches("[a-zA-Z]+")){
            System.out.println("Please use English letters! ");
            teamlead = sc.nextLine();
            continue;
        }
        while (teamlead.length() > 15){
            System.out.println("This name is too long! ");
            teamlead = sc.nextLine();
            continue;
        }
        while (teamlead.length() < 2){
            System.out.println("This name is too short! ");
            teamlead = sc.nextLine();
            continue;
        }
        while (teamlead.isEmpty()){
            System.out.println("The input field is empty! ");
            teamlead = sc.nextLine();
            continue;
        }
        System.out.println("Please, enter your name: ");
        String assistant = sc.nextLine();
        while (!assistant.matches("[a-zA-Z]+")){
            System.out.println("Please use English letters! ");
            assistant = sc.nextLine();
            continue;
        }
        while (assistant.length() >15){
            System.out.println("This name is too long! ");
            assistant = sc.nextLine();
            continue;
        }
        while (assistant.length() < 2){
            System.out.println("This name is too short! ");
            assistant = sc.nextLine();
            continue;
        }
        while (assistant.isEmpty()){
            System.out.println("The input field is empty! ");
            assistant = sc.nextLine();
            continue;
        }
        System.out.println("Please, enter the number of messages: ");

        int value;
        while(true) {
            try {
                value = Integer.parseInt(sc.nextLine());
                if (value > 0 && value < 2147483647) {
                    break;
                }

                System.out.println("Please enter an integer! ");
            } catch (NumberFormatException var6) {
                System.out.println("Please enter an integer! ");
            }
        }

        System.out.println("Привет, " + teamlead + " это твой помощник " + assistant + ".");
        System.out.println("У тебя " + value + " новых сообщений.");
    }
}
